import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Color color = Theme.of(context).primaryColor;
    Widget titleSection = Container(
      padding: const EdgeInsets.all(32),
      child: Row(
        children: [
          Expanded(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  padding: const EdgeInsets.only(bottom: 8),
                  child: Text('This is my favorite movie / saga', style: TextStyle(
                    fontWeight: FontWeight.bold,
                  ),),
                ),
                Text("May the force be with you", style: TextStyle(color: Colors.grey[500], fontStyle: FontStyle.italic),)
              ],
            ),
          ),
          Icon(Icons.arrow_upward, color: Colors.red[500],),
          Text("99")
        ],
      ),
    );
    Widget buttonSelection = Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: [
          _buildButtonColor(color, Icons.sentiment_very_satisfied, 'Happy'),
          _buildButtonColor(color, Icons.personal_video, 'TV'),
          _buildButtonColor(color, Icons.computer, 'Computer'),
        ],
      ),
    );
    Widget textSection = Container(
      padding: const EdgeInsets.all(32),
      child: Text("My father showed me the first movie when I was around 3-4 years old."
                  " My favorite movie in the saga is the 3rd one where we understand who is Darth Vader."
                  " Nevertheless, when Disney bought LucasFilm and produced 3 others movies (without counting special-editions)"
                  " I did not appreciate them while the special effects are pretty good.",
                  softWrap: true,),
    );
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'Lauyout Homework',
      home: Scaffold(
        appBar: AppBar(
          title: Text('Layout Homework'),
        ),
        body: ListView(
            children: [
              Image.asset("assets/star_wars.jpg"),
              titleSection,
              buttonSelection,
              textSection,
            ],
          ),
        )
    );
  }

  Column _buildButtonColor(Color color, IconData iconData, String label) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Icon(iconData, color: color,),
        Container(
          margin: const EdgeInsets.only(top: 8),
          child: Text(label, style: TextStyle(fontSize: 12, fontWeight: FontWeight.w400, color: color),),
        ),
      ],
    );
  }
}